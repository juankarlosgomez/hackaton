import { LitElement, html, css} from 'lit-element'; //por defecto mira en node-modules
import '../heroes-ficha-listado/heroes-ficha-listado.js';
import '../heroes-dm/heroes-dm.js';
import '../heroes-form/heroes-form.js';
import '../heroes-lucha-resultado/heroes-lucha-resultado.js';
import '@granite-elements/granite-spinner/granite-spinner.js';
import 'lit-toast/lit-toast.js'

class HeroesMain extends LitElement {

    static get properties() {
        return {
            heroe: {type: Array},
            showHeroesForm: { type: Boolean },
            showSpinner: { type: Boolean },
            showFight: { type: Boolean },
            heroesFight: {type: Array},
            emptyHeroes: { type: Boolean }
        };
    }

    constructor(){
        super(); //Llamamos al constuctor de la clase base, en este caso de LitElements.
        this.heroe = [];
        this.heroesFight = [];

        this.showHeroesForm = false;
        this.showFight = false;
        this.showSpinner = true;
        this.emptyHeroes = true;

        console.log("Forzamos llamada a dm para recuperar datos");
    }

    static get styles() {
      return css`
        lit-toast.error {
          --lt-background-color: rgba(224, 130, 131, 0.8);
          --lt-color: black;
        }

        lit-toast {
          --lt-background-color: rgba(123, 239, 178, 0.8);
          --lt-color: black;
        }
      `;
    }

    render() {

        //@change se ejecuta al perder el foco del campo
        //@input se ejecuta al teclear
        //map se puede usar para recorrer un array
        //la propiedad photo en vez de pasarlo como atributo, lo pasamos como propiedad indicando un . por
        //delante, de esa forma el parseador de LitE es capaz de parsearlo como objeto
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <!--<h2 class="text-center text-white font-weight-bold">Heros & Villains</h2>-->
            ${this.showSpinner ? html`
            <granite-spinner ?active="${this.showSpinner}"></granite-spinner>
            ` : html`
            <div class="row" id="heroesList">
              ${this.heroe.map( heroe => html `
                <div class="col-12 col-md-6 col-lg-4 pb-3">
                  <heroes-ficha-listado id="ficha${heroe.id}" style="margin-bottom:1em"
                      @heroes-ficha-listado-add-favourite="${this.addFavourite}"
                      @heroes-ficha-listado-remove-favourite="${this.removeFavourite}"
                      @click="${this.heroesDetail}"
                      .heroe="${heroe}" ?isFavourite="${heroe.fav}"
                  >
                    <div class="input-group-text">
                      <input id="chkFight${heroe.id}" type="checkbox" @click="${this.prepareToFight}" ?checked=${false}>
                      &nbsp;
                      <label> Prepare to fight</label>
                    </div>
                  </heroes-ficha-listado>
                </div>
              `
              )}
            </div>
            <div class="row mx-2">
                <heroes-form id="heroesForm" class="rounded border-primary d-none"
                    @heroes-form-close="${this.heroesFormClose}"
                    @heroes-form-store="${this.heroesFormStore}"></heroes-form>
            </div>
            <div class="row mx-2">
                <heroes-lucha-resultado id="heroesFight" class="d-none"></heroes-lucha-resultado>
            </div>
            <div class="w-25 p-3">
                  <lit-toast id="toast"></lit-toast>
            </div>
            `}
            <heroes-dm id="dmHeroes"
                @heroes-dm-saveFavHeroesOrVillain-successful="${this.UpdateHeroeFichaListadoAddFav}"
                @heroes-dm-removeFavHeroesOrVillain-successful="${this.RemoveHeroeFichaListadoRemoveFav}"
                @heroes-dm-addNewHeroOrVillain-successful="${this.reloadAllHeroesVillanos}"
                @heroes-dm-editHeroOrVillain-successful="${this.reloadAllHeroesVillanos}"
                @heroes-dm-getById-response="${this.heroesDetailForm}"
                @heroes-dm-getFavsHeroesOrVillain-no-favs="${this.noFavs}"
                @heroes-dm-getFavsHeroesOrVillain-response="${this.populateData}"
                @heroes-dm-getAllHeroesVillanos-response="${this.populateData}"
                @heroes-dm-getAllHeroes-response="${this.populateData}"
                @heroes-dm-getAllVillanos-response="${this.populateData}"
                @heroes-dm-getByName-response="${this.populateData}"
            ></heroes-dm>
        `;
    }

    /**
     * toast.show(message, [duration])
     * message: String
     * duration: Number in ms
     *
     * return: Promise<void>, resolved when the toast has finished
     */
    _showToast(message, error) {
      if(this.shadowRoot.querySelector('lit-toast')){
        this.shadowRoot.querySelector('lit-toast').show(message, 2000);
        if(error === true){
          this.shadowRoot.querySelector('lit-toast').classList.add("error");
        }else{
          this.shadowRoot.querySelector('lit-toast').classList.remove("error");
        }
      }
    }

    /**
     * Funcion para controla la existencia de unicamente DOS luchadores y guardar sus ID
     *
     * @param {*} e
     */
    prepareToFight(e){
      console.log("prepareToFight en heroes-main");
      e.cancelBubble = true;
      if(this.heroesFight.length <= 2){
        if(this.heroesFight.includes(e.target.id)){
          this.heroesFight = this.heroesFight.filter(function(value, index, arr){
            return value !== e.target.id;
          });
          e.target.checked = false;
        }else{
          if(this.heroesFight.length < 2){
            this.heroesFight.push(e.target.id);
            e.target.checked = true;
          }else{
            e.target.checked = false;
          }
        }
      }else{
        e.target.checked = false;
      }
    }

    addFavourite(e){
        console.log("addFavourite en heroes-main" );
        this.shadowRoot.getElementById("dmHeroes").saveFavHeroesOrVillain(e.detail.id);
    }

    removeFavourite(e){
        console.log("removeFavourite en heroes-main");
        this.shadowRoot.getElementById("dmHeroes").removeFavHeroesOrVillain(e.detail.id);
    }

    UpdateHeroeFichaListadoAddFav(e){
        console.log("UpdateHeroeFichaListadoAddFav en heroes-main");
        this.shadowRoot.getElementById("ficha" + e.detail).isFavourite = true;
        this._showToast("Añadido favorito", false);
    }

    RemoveHeroeFichaListadoRemoveFav(e){
        console.log("RemoveHeroeFichaListadoRemoveFav en heroes-main");
        this.shadowRoot.getElementById("ficha" + e.detail).isFavourite = false;
        this._showToast("Eliminado favorito", false);
    }

    requestAll(){
      this.shadowRoot.getElementById("dmHeroes").getAllHeroesVillanos();
    }

    requestAllVillanos(){
      this.shadowRoot.getElementById("dmHeroes").getAllVillanos();
    }

    requestAllHeroes(){
      this.shadowRoot.getElementById("dmHeroes").getAllHeroes();
    }

    requestByName(e){
      this.shadowRoot.getElementById("dmHeroes").getByName(e.detail.filter.fValue);
    }

    noFavs(e){
      this._showToast("No existen favoritos", false);
      this.showFight = false;
      this.showHeroesForm = false;
      if (e.detail.heroes) {
        this.heroe = [...e.detail.heroes];
      }
    }

    populateData(e){
        console.log("populateData en heroes-main");
        if (e.detail.heroes) {
            this.heroe = [...e.detail.heroes];
        }

        switch (e.type) {
          case 'heroes-dm-getAllVillanos-response':
            this._showToast("Mostrando todos los villanos", false);
            break;
          case 'heroes-dm-getAllHeroes-response':
            this._showToast("Mostrando todos los heroes", false);
            break;
          case 'heroes-dm-getAllHeroesVillanos-response':
            this._showToast("Mostrando todos los heroes y villanos", false);
            break;
          case 'heroes-dm-getFavsHeroesOrVillain-response':
            this._showToast("Mostrando favoritos", false);
            break;
          case 'heroes-dm-getByName-response':
            this._showToast("Mostrando resultados filtro nombre", false);
            break;
          default:
            console.log('El tipo de evento no se corresponde a ninguno definido ' + e.type + '.');
        }
    }

    //Evento litElemnts
    updated(changedProperties){
        console.log("updated heroes-main");

        //Para que LitElements detecte un cambio en un array, se debe asignar el array de nuevo
        if (changedProperties.has("heroe")){
            console.log("Han cambiado el valor de la propiedad heroes en heroes-main");

            this.showFight = false;
            this.showHeroesForm = false;

            //Reiniciamos el array de pelea y los checkbox
            this.heroesFight = [];
            var checkboxes = this.shadowRoot.querySelectorAll('input[type="checkbox"]');
            checkboxes.forEach((checkbox) => {
              checkbox.checked = false;
              checkbox.removeAttribute("checked")
            });

            if (this.heroe.length > 0) {
                this.emptyHeroes = false;
            } else {
                this.emptyHeroes = true;
            }

            this.dispatchEvent(new CustomEvent("hero-updated", {
                detail: {
                  emptyHeroes: this.emptyHeroes
                }
            }));

            this.transition();
        }

        if(changedProperties.has('showHeroesForm')) {
            console.log('heroes-main - updated - Propiedad showHeroesForm ha cambiado de valor');
            console.log(this.showHeroesForm);
            if(this.showHeroesForm === true) {
                this.showHeroesFormData();
            } else {
                this.showHeroesList();
            }
        }

        if(changedProperties.has('showFight')) {
          console.log('heroes-main - updated - Propiedad showFight ha cambiado de valor');
          console.log(this.showFight);
          if(this.showFight === true){
            this.showHeroesFightPage();
          }else{
            if(this.showHeroesForm === true) {
              this.showHeroesFormData();
            } else {
                this.showHeroesList();
            }
          }
      }
    }

    showFavourites(){
      this.shadowRoot.querySelector('heroes-dm').getFavsHeroesOrVillain();
    }

    /**
     *Function to make sleep on the fetch heroes functions to see more transition
     *
     */
    async transition(){
      await this._sleep(2000);

      this.showSpinner = false;
    }

    /**
    *Function to make sleep on the fetch heroes functions to see more transition
    *
    * @param {*} ms
    * @returns
    * @memberof HomePage
    */
    _sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }

    heroesFormClose(e) {
        console.log('heroes-main - heroesFormClose');
        this.showHeroesForm = false;
    }

    heroesFormStore(e) {
        console.log('heroes-main - heroesFormStore');
        console.log(e.detail.hero);
        console.log('heroes-main - heroesFormStore - editingHero - ' + e.detail.editingHero);

        if (e.detail.hero) {
          if(e.detail.editingHero && e.detail.editingHero === true){
            this.shadowRoot.getElementById("dmHeroes").editHeroOrVillain(e.detail.hero);
          }else{
            this.shadowRoot.getElementById("dmHeroes").addNewHeroOrVillain(e.detail.hero);
          }
        }

        this.shadowRoot.getElementById("heroesForm").resetFormData();

        this.showHeroesForm = false;
    }

    reloadAllHeroesVillanos(e) {
        this.shadowRoot.getElementById("dmHeroes").getAllHeroesVillanos();

        switch (e.type) {
          case 'heroes-dm-editHeroOrVillain-successful':
            this._showToast("Registro editado", false);
            break;
          case 'heroes-dm-addNewHeroOrVillain-successful':
            this._showToast("Registro añadido", false);
            break;
          default:
            console.log('El tipo de evento no se corresponde a ninguno definido ' + e.type + '.');
        }
    }

    heroesDetail(e) {
        console.log('heroes-main - heroesDetail');
        console.log(e.target.id);
        if(e.target.id) {
            this.shadowRoot.getElementById("dmHeroes").getById((e.target.id).replace('ficha', ''));
        }
    }

    heroesDetailForm(e) {
        console.log('heroes-main - heroesDetail');
        console.log(e.detail);
        if(e.detail.heroes) {
            this.shadowRoot.getElementById("heroesForm").hero = e.detail.heroes[0];
            this.shadowRoot.getElementById("heroesForm").editingHero = true;
            this.showHeroesForm = true;
            this._showToast("Editando registro", false);
        }
    }

    showHeroesList(){
        console.log("showHeroesList");
        if( this.shadowRoot.getElementById("heroesForm")){
          this.shadowRoot.getElementById("heroesForm").classList.add("d-none");
          this.shadowRoot.getElementById("heroesList").classList.remove("d-none");
        }
        if(this.shadowRoot.getElementById("heroesFight")){
          this.shadowRoot.getElementById("heroesFight").classList.add("d-none");
        }
    }

    showHeroesFormData(){
        console.log("showHeroesFormData");
        if( this.shadowRoot.getElementById("heroesForm")){
          this.shadowRoot.getElementById("heroesForm").classList.remove("d-none");
          this.shadowRoot.getElementById("heroesList").classList.add("d-none");
        }
        if(this.shadowRoot.getElementById("heroesFight")){
          this.shadowRoot.getElementById("heroesFight").classList.add("d-none");
        }
    }

    showHeroesFightPage(){
      console.log("showHeroesFormData");
      if( this.shadowRoot.getElementById("heroesForm")){
        this.shadowRoot.getElementById("heroesForm").classList.add("d-none");
      }
      if(this.shadowRoot.getElementById("heroesList")){
        this.shadowRoot.getElementById("heroesList").classList.add("d-none");
      }

      if(this.shadowRoot.getElementById("heroesFight")){
        this.shadowRoot.getElementById("heroesFight").classList.remove("d-none");
      }
  }

    startFight(e) {
      console.log('heroes-main - startFight');

      if(this.heroesFight.length !== 2){
        this._showToast("Debe seleccionar dos participantes para iniciar una pelea", true);
      }else{
        this.showFight = true;

        var heroe1 = this.shadowRoot.getElementById(this.heroesFight[0].replace('chkFight','ficha')).heroe;
        var heroe2 = this.shadowRoot.getElementById(this.heroesFight[1].replace('chkFight','ficha')).heroe;

        this.shadowRoot.getElementById("heroesFight").heroe1 = {};
        this.shadowRoot.getElementById("heroesFight").heroe2 = {};

        this.shadowRoot.getElementById("heroesFight").heroe1 = heroe1;
        this.shadowRoot.getElementById("heroesFight").heroe2 = heroe2;
      }
    }
}

customElements.define('heroes-main', HeroesMain);
