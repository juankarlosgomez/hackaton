import { LitElement, html } from 'lit-element';

class HeroesDM extends LitElement {

  static get properties(){
    return {
      heroesVillanos: { type: Array }
    };
  }

  constructor() {
    super();

    this.fetchAllHeroesVillanos();
  }

  updated(changedProperties) {
    console.log("heroes-dm, updated");

    if(changedProperties.has("heroesVillanos")){
      console.log("heroes-dm, propiedad heroesVillanos ha cambiado");

      this.dispatchEvent(
        new CustomEvent(
          "heroes-dm-getAllHeroesVillanos-response",{
            detail: {
              heroes: this.heroesVillanos
            }
          }
        )
      );
    }
  }

  /**
   * Metodo para alimentación inicial de Heroes y Villanos
   */
  fetchAllHeroesVillanos() {
    console.log("ini fetchAllHeroesVillanos");

    let xhr = new XMLHttpRequest();

    xhr.onload = () => {
      if(xhr.status === 200){
        console.log("Petición completada correctamente");

        let APIResponse = JSON.parse(xhr.responseText);

        //Cortamos tamaño monstruoso de array
        APIResponse = APIResponse.slice(0,8);

        //Si on existen heroes en heroesLocalStorage nos quedamos con los del servicio, en caso contrario concatenaremos arrays
        if(window.localStorage.getItem('heroesLocalStorage')){
          var heroesLocalStorageTMP = JSON.parse(window.localStorage.getItem('heroesLocalStorage'));
          //Si hay algun heroe con un id en el localstorage lo eliminamos para que prevalezca el editado
          APIResponse = APIResponse.filter(a => !heroesLocalStorageTMP.map(b => b.id).includes(a.id));
          this.heroesVillanos = [...APIResponse, ...heroesLocalStorageTMP];
          this.heroesVillanos.sort((a, b) => (a.id > b.id) ? 1 : -1);
        }else{
          this.heroesVillanos = APIResponse;
        }

        if(window.localStorage.getItem('heroesFavLocalStorage')){
          var heroesFavLocalStorageTMP = JSON.parse(window.localStorage.getItem('heroesFavLocalStorage'));

          this.heroesVillanos.forEach(function (heroe) {
            heroe.fav = (heroesFavLocalStorageTMP.includes(parseInt(heroe.id))) ? true : false;
          });
        }else{
          this.heroesVillanos.forEach(function (heroe) {
            heroe.fav = false;
          });
        }
      }
    };

    xhr.open("GET", "https://cdn.jsdelivr.net/gh/akabab/superhero-api@0.3.0/api/all.json");
    xhr.send();

    console.log("fin fetchAllHeroesVillanos");
  }

  /**
   * Metodo para obtener TODOS los Heroes y Villanos se lanzara desde el componente que contenga el DM
   * de la siguiente manera:
   *
   * this.shadowRoot.querySelector("heroes-dm").getAllHeroesVillanos();
   *
   * Y se lanzara un evento heroes-dm-getAllHeroesVillanos-response como respuesta
   */
  getAllHeroesVillanos(){
    console.log("ini getAllHeroesVillanos");
    this.dispatchEvent(
      new CustomEvent(
        "heroes-dm-getAllHeroesVillanos-response",{
          detail: {
            heroes: this.heroesVillanos
          }
        }
      )
    );
    console.log("fin getAllHeroesVillanos");
  }

  /**
   * Metodo para obtener TODOS los Heroes, se lanzara desde el componente que contenga el DM de la
   * siguiente manera:
   *
   * this.shadowRoot.querySelector("heroes-dm").getAllHeroes();
   *
   * Y se lanzara un evento heroes-dm-getAllHeroes-response como respuesta
   */
  getAllHeroes(){
    console.log("ini getAllHeroes");
    var heroes = this.heroesVillanos.filter(
      heroe => heroe.biography.alignment === "good"
    )

    this.dispatchEvent(
      new CustomEvent(
        "heroes-dm-getAllHeroes-response",{
          detail: {
            heroes: heroes
          }
        }
      )
    );
    console.log("fin getAllHeroes");
  }

    /**
   * Metodo para obtener TODOS los Villanos, se lanzara desde el componente que contenga el DM de la
   * siguiente manera:
   *
   * this.shadowRoot.querySelector("heroes-dm").getAllVillanos();
   *
   * Y se lanzara un evento heroes-dm-getAllVillanos-response como respuesta
   */
  getAllVillanos(){
    console.log("ini getAllVillanos");
    var villanos = this.heroesVillanos.filter(
      villano => villano.biography.alignment !== "good"
    )

    this.dispatchEvent(
      new CustomEvent(
        "heroes-dm-getAllVillanos-response",{
          detail: {
            heroes: villanos
          }
        }
      )
    );
    console.log("fin getAllVillanos");
  }

  /**
   * Metodo que devuelve el superheroe o villano asociado al id recibido como parametro, se
   * lanzara desde el componente que contenga el DM de la siguiente manera:
   *
   * this.shadowRoot.querySelector("heroes-dm").getById(X);
   *
   * Y se lanzara un evento heroes-dm-getById-response como respuesta
   *
   * @param {} id - Identificador del superheroe a encontrar
   */
  getById(id){
    console.log("ini getById");
    var heroOrVillain = this.heroesVillanos.filter(
      heroOrVillain => parseInt(heroOrVillain.id) === parseInt(id)
    )
    this.dispatchEvent(
      new CustomEvent(
        "heroes-dm-getById-response",{
          detail: {
            heroes: heroOrVillain
          }
        }
      )
    );
    console.log("ini getById");
  }

  /**
   * Metodo para obtener todos los heroes cuyo nombre contengan el string recibido por parametro, se
   * lanzara desde el componente que contenga el DM de la siguiente manera:
   *
   * this.shadowRoot.querySelector("heroes-dm").getByName(X);
   *
   * Y se lanzara un evento heroes-dm-getById-response como respuesta
   *
   * @param {*} nameOrPart - Nombre o parte del nombre que debe coincidir con el de los heroes
   */
  getByName(nameOrPart){
    console.log("ini getByName");
    var heroOrVillain = this.heroesVillanos.filter(
      heroOrVillain => heroOrVillain.name.toLowerCase().indexOf(nameOrPart.toLowerCase()) !== -1
    )
    this.dispatchEvent(
      new CustomEvent(
        "heroes-dm-getByName-response",{
          detail: {
            heroes: heroOrVillain
          }
        }
      )
    );
    console.log("ini getById");
  }

  /**
   * Metodo para añadir nuevos registros de villanos o Heroes, se grabaran ademas los nuevos registros en
   * el localStorage para recuperarlos en futuros usos de la aplicación, se lanzara desde el componente
   * que contenga el DM de la siguiente manera:
   *
   * this.shadowRoot.querySelector("heroes-dm").addNewHeroOrVillain([Objeto que contenga al heroe]);
   *
   * Se lanzara el evento heroes-dm-addNewHeroOrVillain-successful en caso de alta con exito, por el
   * contrario se lanzara heroes-dm-addNewHeroOrVillain-failed
   *
   * @param {} heroOrVillain
   */
  addNewHeroOrVillain(heroOrVillain){
    console.log("ini addNewHeroOrVillain");

    try{
      var heroOrVillainTMP = heroOrVillain;
      heroOrVillainTMP.id = parseInt(getMaxId(this.heroesVillanos));
      //Guradaremos el heroe en el localStorage
      if(window.localStorage.getItem('heroesLocalStorage')){
        //Si habia previamente heroesLocalStorage recogemos el array, le añadimos el nuevo registro y lo grabamos de nuevo en heroesLocalStorage
        var heroesLocalStorageTMP = JSON.parse(window.localStorage.getItem('heroesLocalStorage'));
        heroesLocalStorageTMP.push(heroOrVillainTMP);
        window.localStorage.setItem('heroesLocalStorage', JSON.stringify(heroesLocalStorageTMP));
      }else{
        //Si no habia previamente heroesLocalStorage grabados generaremos un array con el primer registro
        window.localStorage.setItem('heroesLocalStorage', JSON.stringify([heroOrVillainTMP]));
      }

      //Actualziamos tambien los heroes en memoria
      this.heroesVillanos.push(heroOrVillainTMP);

      this.dispatchEvent(new CustomEvent("heroes-dm-addNewHeroOrVillain-successful",{}));
    }catch(e){
      this.dispatchEvent(new CustomEvent("heroes-dm-addNewHeroOrVillain-failed",{}));
    }
    console.log("fin addNewHeroOrVillain");
  }

  editHeroOrVillain(heroOrVillain){
    console.log("ini editHeroOrVillain");
    try{
      this.heroesVillanos = this.heroesVillanos.filter(
        heroOrVillainTmp => heroOrVillainTmp.id !== heroOrVillain.id
      )
      this.heroesVillanos.push(heroOrVillain);
      this.heroesVillanos.sort((a, b) => (a.id > b.id) ? 1 : -1);
      //Guradaremos el heroe en el localStorage
      if(window.localStorage.getItem('heroesLocalStorage')){

        var heroesLocalStorageTMP = JSON.parse(window.localStorage.getItem('heroesLocalStorage'));

        heroesLocalStorageTMP = heroesLocalStorageTMP.filter(
          heroOrVillainTmp => heroOrVillainTmp.id !== heroOrVillain.id
        )

        heroesLocalStorageTMP.push(heroOrVillain);
        window.localStorage.setItem('heroesLocalStorage', JSON.stringify(heroesLocalStorageTMP));

      }else{
        //Si no habia previamente heroesLocalStorage grabados generaremos un array con el primer registro
        window.localStorage.setItem('heroesLocalStorage', JSON.stringify([heroOrVillain]));
      }
      this.dispatchEvent(new CustomEvent("heroes-dm-editHeroOrVillain-successful",{}));
    }catch(e){
      this.dispatchEvent(new CustomEvent("heroes-dm-editHeroOrVillain-failed",{}));
    }
    console.log("fin editHeroOrVillain");
  }

  /**
   * Metodo para obtener un array de heroes en funcion de los id guardados en localstorage heroesFavLocalStorage,
   * se lanzara desde el componente que contenga el DM de la siguiente manera:
   *
   * this.shadowRoot.querySelector("heroes-dm").getFavsHeroesOrVillain();
   *
  * Si existian datos en el localStorage se remitira un array con los heroes y villanos favoritos mdiante el evento:
  *
  * heroes-dm-getFavsHeroesOrVillain-response
  *
  * Si por el contratio no teniamos favoritas guardados se lanzara el evento:
  *
  * heroes-dm-getFavsHeroesOrVillain-no-favs
  *
   */
  getFavsHeroesOrVillain(){
    console.log("ini getFavsHeroesOrVillain");

    //Si on existen heroes en heroesLocalStorage nos quedamos con los del servicio, en caso contrario concatenaremos arrays
    if(window.localStorage.getItem('heroesFavLocalStorage')){
      var heroesFavLocalStorageTMP = JSON.parse(window.localStorage.getItem('heroesFavLocalStorage'));
      var arrayOfFavs = this.heroesVillanos.filter(
        heroOrVillain => heroesFavLocalStorageTMP.includes(heroOrVillain.id)
      );

      this.dispatchEvent(
        new CustomEvent(
          "heroes-dm-getFavsHeroesOrVillain-response",{
            detail: {
              heroes: arrayOfFavs
            }
          }
        )
      );
    }else{
      this.dispatchEvent(new CustomEvent("heroes-dm-getFavsHeroesOrVillain-no-favs",{ detail: {heroes: []}}));
    }

    console.log("fin getFavsHeroesOrVillain");
  }

  /**
   * Metodo para guardar en el local storage el id del heroe o villano que queremos marcar como favorito,
   * se lanzara desde el componente que contenga el DM de la siguiente manera:
   *
   * this.shadowRoot.querySelector("heroes-dm").saveFavHeroesOrVillain(X);
   *
   * Se lanzara el evento heroes-dm-saveFavHeroesOrVillain-successful en caso de alta con exito, por el
   * contrario se lanzara heroes-dm-saveFavHeroesOrVillain-failed
   *
   * @param {} idFav
   */
  saveFavHeroesOrVillain(idFav){
    console.log("ini saveFavHeroesOrVillain");

    try{
      if(window.localStorage.getItem('heroesFavLocalStorage')){
        //Si habia previamente heroesFavLocalStorage recogemos el array, le añadimos el nuevo registro y lo grabamos de nuevo en heroesFavLocalStorage
        var heroesFavLocalStorageTMP = JSON.parse(window.localStorage.getItem('heroesFavLocalStorage'));
        if(!heroesFavLocalStorageTMP.includes(parseInt(idFav))){
          heroesFavLocalStorageTMP.push(parseInt(idFav));
        }
        this.heroesVillanos.forEach(function (heroe) {
          heroe.fav = (heroesFavLocalStorageTMP.includes(parseInt(heroe.id))) ? true : false;
        });
        window.localStorage.setItem('heroesFavLocalStorage', JSON.stringify(heroesFavLocalStorageTMP));
      }else{
        //Si no habia previamente heroesFavLocalStorage grabados generaremos un array con el primer registro
        window.localStorage.setItem('heroesFavLocalStorage', JSON.stringify([parseInt(idFav)]));
      }
      this.dispatchEvent(new CustomEvent("heroes-dm-saveFavHeroesOrVillain-successful",{detail : parseInt(idFav)}));
    }catch(e){
      this.dispatchEvent(new CustomEvent("heroes-dm-saveFavHeroesOrVillain-failed",{detail : parseInt(idFav)}));
    }

    console.log("fin saveFavHeroesOrVillain");
  }

    /**
   * Metodo para borrar en el local storage el id del heroe o villano que queremos marcar como no favorito,
   * se lanzara desde el componente que contenga el DM de la siguiente manera:
   *
   * this.shadowRoot.querySelector("heroes-dm").removeFavHeroesOrVillain(X);
   *
   * Se lanzara el evento heroes-dm-removeFavHeroesOrVillain-successful en caso de alta con exito, por el
   * contrario se lanzara heroes-dm-removeFavHeroesOrVillain-failed
   *
   * @param {} idFav
   */
  removeFavHeroesOrVillain(idFav){
    console.log("ini removeFavHeroesOrVillain");

    if(window.localStorage.getItem('heroesFavLocalStorage')){
      //Si habia previamente heroesFavLocalStorage recogemos el array, le añadimos el nuevo registro y lo grabamos de nuevo en heroesFavLocalStorage
      var heroesFavLocalStorageTMP = JSON.parse(window.localStorage.getItem('heroesFavLocalStorage'));

      var finalArrayFav = heroesFavLocalStorageTMP.filter(
        heroe => parseInt(heroe) !== parseInt(idFav)
      )
      this.heroesVillanos.forEach(function (heroe) {
        heroe.fav = (finalArrayFav.includes(parseInt(heroe.id))) ? true : false;
      });

      if(finalArrayFav.length == 0){
        window.localStorage.removeItem('heroesFavLocalStorage');
      }else{
        window.localStorage.setItem('heroesFavLocalStorage', JSON.stringify(finalArrayFav));
      }
      this.dispatchEvent(new CustomEvent("heroes-dm-removeFavHeroesOrVillain-successful",{detail : parseInt(idFav)}));
    }else{
      this.dispatchEvent(new CustomEvent("heroes-dm-removeFavHeroesOrVillain-failed",{detail : parseInt(idFav)}));
    }

    console.log("fin removeFavHeroesOrVillain");
  }

}

/**
 * Función para calcular el ID maximo en el array de heroes y villanos para usarlos en nuevas altas
 *
 * @param {} heroesVillanos Array del que inspeccionaremos el ID maximo
 * @returns
 */
function getMaxId(heroesVillanos){
  return Math.max.apply(Math, heroesVillanos.map(function(o) { return o.id; })) + 1;
}

customElements.define('heroes-dm', HeroesDM);
