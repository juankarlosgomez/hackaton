import { LitElement, html, css } from 'lit-element';

class HeroesForm extends LitElement {

    static get styles() {
        return css`
          :host {
            background-color: white;
          }
        `;
      }

    static get properties() {
        return {
            hero: { type: Object },
            alignmentIsGood: { type: Boolean },
            editingHero: { type: Boolean }
        };
    }

    constructor() {
        super();

        this.resetFormData();
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group mt-3">
                        <label class="form-label">Name</label>
                        <input type="text" class="form-control" placeholder="Name"
                            @input="${this.updateName}"
                            .value="${this.hero.name}"
                            ?disabled="${this.editingHero}" />
                    </div>
                    <div class="form-group mt-3">
                        <label class="form-label fw-bold fs-4">Power stats</label>
                        <div class="row row-cols-1 row-cols-sm-6">
                            <div class="form-group mt-3">
                                <label class="form-label">
                                    Intelligence - 
                                    <span class="${this.hero.powerstats.intelligence <= 30 ?
                                        "text-danger" :
                                        this.hero.powerstats.intelligence > 30 && this.hero.powerstats.intelligence < 70 ?
                                            "text-warning":
                                            "text-success"}">
                                        <strong>${this.hero.powerstats.intelligence}</strong>
                                    </span>
                                </label>
                                <input type="range" class="form-range" min="0" max="100"
                                    .value="${this.hero.powerstats.intelligence}"
                                    @change="${this.intelligenceChange}" />
                            </div>
                            <div class="form-group mt-3">
                                <label class="form-label">
                                    Strength - 
                                    <span class="${this.hero.powerstats.strength <= 30 ?
                                        "text-danger" :
                                        this.hero.powerstats.strength > 30 && this.hero.powerstats.strength < 70 ?
                                            "text-warning":
                                            "text-success"}">
                                        <strong>${this.hero.powerstats.strength}</strong>
                                    </span>
                                </label>
                                <input type="range" class="form-range" min="0" max="100"
                                    .value="${this.hero.powerstats.strength}"
                                    @change="${this.strengthChange}" />
                            </div>
                            <div class="form-group mt-3">
                                <label class="form-label">
                                    Speed - 
                                    <span class="${this.hero.powerstats.speed <= 30 ?
                                        "text-danger" :
                                        this.hero.powerstats.speed > 30 && this.hero.powerstats.speed < 70 ?
                                            "text-warning":
                                            "text-success"}">
                                        <strong>${this.hero.powerstats.speed}</strong>
                                    </span>
                                </label>
                                <input type="range" class="form-range" min="0" max="100"
                                    .value="${this.hero.powerstats.speed}"
                                    @change="${this.speedChange}" />
                            </div>
                            <div class="form-group mt-3">
                                <label class="form-label">
                                    Durability - 
                                    <span class="${this.hero.powerstats.durability <= 30 ?
                                        "text-danger" :
                                        this.hero.powerstats.durability > 30 && this.hero.powerstats.durability < 70 ?
                                            "text-warning":
                                            "text-success"}">
                                        <strong>${this.hero.powerstats.durability}</strong>
                                    </span>
                                </label>
                                <input type="range" class="form-range" min="0" max="100"
                                    .value="${this.hero.powerstats.durability}"
                                    @change="${this.durabilityChange}" />
                            </div>
                            <div class="form-group mt-3">
                                <label class="form-label">
                                    Power - 
                                    <span class="${this.hero.powerstats.power <= 30 ?
                                        "text-danger" :
                                        this.hero.powerstats.power > 30 && this.hero.powerstats.power < 70 ?
                                            "text-warning":
                                            "text-success"}">
                                        <strong>${this.hero.powerstats.power}</strong>
                                    </span>
                                </label>
                                <input type="range" class="form-range" min="0" max="100"
                                    .value="${this.hero.powerstats.power}"
                                    @change="${this.powerChange}" />
                            </div>
                            <div class="form-group mt-3">
                                <label class="form-label">
                                    Combat - 
                                    <span class="${this.hero.powerstats.combat <= 30 ?
                                        "text-danger" :
                                        this.hero.powerstats.combat > 30 && this.hero.powerstats.combat < 70 ?
                                            "text-warning":
                                            "text-success"}">
                                        <strong>${this.hero.powerstats.combat}</strong>
                                    </span>
                                </label>
                                <input type="range" class="form-range" min="0" max="100"
                                    .value="${this.hero.powerstats.combat}"
                                    @change="${this.combatChange}" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <label class="form-label fw-bold fs-4">Biography</label>
                        <div class="row row-cols-1 row-cols-sm-3">
                            <div class="form-group mt-3">
                                <label class="form-label">Full Name</label>
                                <input type="text" class="form-control" placeholder="Full Name"
                                    @input="${this.updateFullName}"
                                    .value="${this.hero.biography.fullName}" />
                            </div>
                            <div class="form-group mt-3">
                                <label class="form-label">Alter Egos</label>
                                <input type="text" class="form-control" placeholder="Alter Egos"
                                    @input="${this.updateAlterEgos}"
                                    .value="${this.hero.biography.alterEgos}" />
                            </div>
                            <div class="form-group mt-3">
                                <label class="form-label">Alias</label>
                                <input type="text" class="form-control" placeholder="Alias"
                                    @input="${this.updatAliases}"
                                    .value="${this.hero.biography.aliases}" />
                            </div>
                            <div class="form-group mt-3">
                                <label class="form-label">Place Of Birth</label>
                                <input type="text" class="form-control" placeholder="Place Of Birth"
                                    @input="${this.updatePlaceOfBirth}"
                                    .value="${this.hero.biography.placeOfBirth}" />
                            </div>
                            <div class="form-group mt-3">
                                <label class="form-label">First Appearance</label>
                                <input type="text" class="form-control" placeholder="First Appearance"
                                    @input="${this.updateFirstAppearance}"
                                    .value="${this.hero.biography.firstAppearance}" />
                            </div>
                            <div class="form-group mt-3">
                                <label class="form-label">Publisher</label>
                                <input type="text" class="form-control" placeholder="Publisher"
                                    @input="${this.updatePublisher}"
                                    .value="${this.hero.biography.publisher}" />
                            </div>
                            <div class="form-group mt-3">
                                <label class="form-label">Alignment</label>
                                <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                                    <input type="radio" class="btn-check" name="btnradio" id="btnradio1" autocomplete="off" value="good"
                                        .checked="${this.alignmentIsGood}"
                                        @change="${this.changeAlignment}">
                                    <label class="btn btn-outline-primary" for="btnradio1">Hero</label>

                                    <input type="radio" class="btn-check" name="btnradio" id="btnradio2" autocomplete="off" value="bad"
                                        .checked="${!this.alignmentIsGood}"
                                        @change="${this.changeAlignment}">
                                    <label class="btn btn-outline-primary" for="btnradio2">Villain</label>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group mt-3 mb-3">
                        <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                        <button @click="${this.storeHero}" class="btn btn-success"><strong>Guardar</strong></button>
                    </div>
                </form>
            </div>
        `;
    }

    updated(changedProperties) {
        console.log('heroes-form - updated');
        if(changedProperties.has('hero')) {
            console.log(`hero-form - updated - Propiedad hero ha cambiado de valor`);
            this.alignmentIsGood = this.hero.biography.alignment !== 'bad';
            console.log('alignmentIsGood -' + this.alignmentIsGood);
        }
    }

    updateName(e) {
        console.log('heroes-form - updateName - Actualizando la propiedad name con el valor ' + e.target.value);
        this.hero.name = e.target.value;
        this.hero = {... this.hero};
    }

    intelligenceChange(e) {
        console.log('heroes-form - intelligenceChange - Actualizando la propiedad intelligence con el valor ' + e.target.value);
        this.hero.powerstats.intelligence = e.target.value;
        this.hero = {... this.hero};
    }

    strengthChange(e) {
        console.log('heroes-form - strengthChange - Actualizando la propiedad strength con el valor ' + e.target.value);
        this.hero.powerstats.strength = e.target.value;
        this.hero = {... this.hero};
    }

    speedChange(e) {
        console.log('heroes-form - speedChange - Actualizando la propiedad speed con el valor ' + e.target.value);
        this.hero.powerstats.speed = e.target.value;
        this.hero = {... this.hero};
    }

    durabilityChange(e) {
        console.log('heroes-form - durabilityChange - Actualizando la propiedad durability con el valor ' + e.target.value);
        this.hero.powerstats.durability = e.target.value;
        this.hero = {... this.hero};
    }

    powerChange(e) {
        console.log('heroes-form - powerChange - Actualizando la propiedad power con el valor ' + e.target.value);
        this.hero.powerstats.power = e.target.value;
        this.hero = {... this.hero};
    }

    combatChange(e) {
        console.log('heroes-form - combatChange - Actualizando la propiedad combat con el valor ' + e.target.value);
        this.hero.powerstats.combat = e.target.value;
        this.hero = {... this.hero};
    }

    updateFullName(e) {
        console.log('heroes-form - updateFullName - Actualizando la propiedad fullName con el valor ' + e.target.value);
        this.hero.biography.fullName = e.target.value;
        this.hero = {... this.hero};
    }

    updateAlterEgos(e) {
        console.log('heroes-form - updateAlterEgos - Actualizando la propiedad alterEgos con el valor ' + e.target.value);
        this.hero.biography.alterEgos = e.target.value;
        this.hero = {... this.hero};
    }

    updatAliases(e) {
        console.log('heroes-form - updatAliases - Actualizando la propiedad aliases con el valor ' + e.target.value);
        this.hero.biography.aliases = e.target.value;
        this.hero = {... this.hero};
    }

    updatePlaceOfBirth(e) {
        console.log('heroes-form - updatePlaceOfBirth - Actualizando la propiedad placeOfBirth con el valor ' + e.target.value);
        this.hero.biography.placeOfBirth = e.target.value;
        this.hero = {... this.hero};
    }

    updateFirstAppearance(e) {
        console.log('heroes-form - updateFirstAppearance - Actualizando la propiedad firstAppearance con el valor ' + e.target.value);
        this.hero.biography.firstAppearance = e.target.value;
        this.hero = {... this.hero};
    }

    updatePublisher(e) {
        console.log('heroes-form - updatePublisher - Actualizando la propiedad publisher con el valor ' + e.target.value);
        this.hero.biography.publisher = e.target.value;
        this.hero = {... this.hero};
    }

    changeAlignment(e) {
        console.log('heroes-form - changeAlignment - Actualizando la propiedad alignment con el valor ' + e.target.value);
        this.hero.biography.alignment = e.target.value;
        this.hero = {... this.hero};
        this.alignmentIsGood = this.hero.biography.alignment !== 'bad';
    }

    goBack(e) {
        console.log('heroes-form - goBack');
        e.preventDefault();

        this.dispatchEvent(new CustomEvent('heroes-form-close', {}));

        this.resetFormData();
    }

    resetFormData() {
        console.log('heroes-form - resetFormData');

        let hero = {};
        hero.id = '';
        hero.name = '';
        hero.powerstats = {};
        hero.powerstats.intelligence = 0;
        hero.powerstats.strength = 0;
        hero.powerstats.speed = 0;
        hero.powerstats.durability = 0;
        hero.powerstats.power = 0;
        hero.powerstats.combat = 0;
        hero.biography = {};
        hero.biography.fullName = '';
        hero.biography.alterEgos = '';
        hero.biography.aliases = '';
        hero.biography.placeOfBirth = '';
        hero.biography.firstAppearance = '';
        hero.biography.publisher = '';
        hero.biography.alignment = 'good';
        hero.images = {};
        hero.images.sm = '';

        this.hero = {... hero};

        this.alignmentIsGood = this.hero.biography.alignment !== 'bad';

        this.editingHero = false;
    }

    storeHero(e) {
        console.log('heroes-form - storeHero');
        e.preventDefault();

        let photo = {
            src: './img/persona.png',
            alt: 'Persona'
        }

        var hero = {
            id : this.hero.id,
            name: this.hero.name,
            powerstats: {
                intelligence: this.hero.powerstats.intelligence,
                strength: this.hero.powerstats.strength,
                speed: this.hero.powerstats.speed,
                durability: this.hero.powerstats.durability,
                power: this.hero.powerstats.power,
                combat: this.hero.powerstats.combat
            },
            biography: {
                fullName: this.hero.biography.fullName,
                alterEgos: this.hero.biography.alterEgos,
                aliases: this.hero.biography.aliases,
                placeOfBirth: this.hero.biography.placeOfBirth,
                firstAppearance: this.hero.biography.firstAppearance,
                publisher: this.hero.biography.publisher,
                alignment: this.hero.biography.alignment
            },
            images: {
                sm: this.alignmentIsGood ? './img/hero.png' : './img/villian.png'
            }
        };

        if(this.hero && this.hero.images && this.hero.images.sm !== ''){
          hero.images = this.hero.images;
        }

        this.dispatchEvent(new CustomEvent('heroes-form-store', {
            detail: {
                hero: hero,
                editingHero: this.editingHero
            }
        }));
    }
}

customElements.define('heroes-form', HeroesForm);
