import { LitElement, html } from 'lit-element'

class HeroesSidebar extends LitElement {

	static get properties() {
		return {
			activePillId: {type: String},
			filter: {type: Object},
			classNameField: {type: String},
			emptyheroes: {type: Boolean}
		};
	}

	//Se puede definir un constructor para dar valor a las propiedades
	constructor() {
		super();
		this.activePillId = "";
		this.filter = {
			fType: "",
			fValue: ""
		};
		this.classNameField = "form-control";
		this.emptyheroes = false;
	}

	render() {
		return html`
			<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
			<aside>
				<section>
					<nav class="nav nav-pills  navbar-light bg-light rounded">
						<ul class="nav flex-column">
							<li class="nav-item">
								<a class="nav-link text-dark bg-light" id="v-pills-choosen-tab" @click="${this.favourites}" href="#">
								Favourites</a>
							</li>
              				<li class="nav-item">
								<a class="nav-link text-dark bg-light" id="v-pills-choosen-tab" @click="${this.filterAll}" href="#">
								All</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-dark bg-light" id="v-pills-filters-tab">
								<strong>Filters</strong></a>
								<ul class="flex-column">
									<li>
										<a class="nav-link text-dark bg-light" id="v-pills-filters-tab-heroes" @click="${this.filterHeroes}" href="#">
										By Heroes</a>
									</li>
									<li>
										<a class="nav-link text-dark bg-light" id="v-pills-filters-tab-villains" @click="${this.filterVillains}" href="#">
										By Villains</a>
									</li>
									<li>
										<div class="row input-group mb-3">
											<div class="col">
												<input type="text" id="fieldNameid" class="${this.classNameField}" placeholder="By name..." required @input="${this.updateName}" .value="${this.filter.fValue}" />
											</div>
											<div class="input-group-prepend col-3 col-md-4">
											<button class="btn btn-outline-secondary" type="button"  @click="${this.filterName}"><strong>Go</strong></button>
											</div>
										</div>
									</li>
								</ul>

							</li>
							<br />
							<li class="nav-item text-center">
								<button class="btn btn-primary btn-sm" @click="${this.newHero}"><strong>Add new Hero</strong></button>
							</li>
              				<br/>
              				<li class="nav-item text-center">
								<button class="btn btn-primary btn-sm" @click="${this.startFight}"><strong>Fight</strong></button>
							</li>
							<br />
						</ul>
					</nav>
					<br/>
          <section class="d-none d-sm-block">
					  <img class="img-thumbnail" alt="Heroes" src="./img/1_m3.jpg">
          </section>
				</section>
			</aside>
		`;
	}

	updateName (e) {
		console.log("updateName en heroes-sidebar")
		console.log("Actualizando la propiedad name con el valor " + e.target.value);
		this.filter.fValue = e.target.value;
		if (!(this.filter.fValue === undefined || this.filter.fValue === "" || this.filter.fValue === null)) {
			if(this.classNameField === "form-control is-invalid") {
				this.classNameField = "form-control is-valid";
			}
		} else {
			this.classNameField = "form-control";
			if (this.emptyHeroes) {
				this.filterAll();
			}
		}
	}

	fireEventFilter (filter) {
		console.log("fireEventFilter en heroes-sidebar")
		console.log("fType: " + this.filter.fType);
		console.log("fValue: " + this.filter.fValue);
		this.dispatchEvent(new CustomEvent("nav-to-filters", {
			detail: {
				filter: this.filter
			}
		}));
	}

	filterHeroes(e) {
		console.log("filterHeroes en heroes-sidebar")
		console.log("Se van a filtrar los heroes");
		this.filter.fType = "H";
		this.filter.fValue = ""
		this.classNameField = "form-control";
		this.fireEventFilter(this.filter);
	}

	filterVillains(e) {
		console.log("filterVillains en heroes-sidebar")
		console.log("Se van a filtrar los villanos");
		this.filter.fType = "V";
		this.filter.fValue = ""
		this.classNameField = "form-control";
		this.fireEventFilter(this.filter);
	}

  filterAll(e) {
		console.log("filterAll en heroes-sidebar")
		console.log("Se van a mostrar todos");
		this.filter.fType = "A";
		this.filter.fValue = ""
		this.classNameField = "form-control";
		this.fireEventFilter(this.filter);
	}

	filterName(e) {
		console.log("filterName en heroes-sidebar")
		console.log("Se van a filtrar por nombre");

		if (this.filter.fValue === undefined || this.filter.fValue === "" || this.filter.fValue === null) {
			this.classNameField = "form-control is-invalid";
		} else {
			this.filter.fType = "N";
			this.fireEventFilter(this.filter);
			this.classNameField = "form-control is-valid";
		}
	}

	favourites(e) {
		console.log("favourites en heroes-sidebar")
		console.log("Se va a acceder a la ventana de favoritos");
		this.classNameField = "form-control";
		this.dispatchEvent(new CustomEvent("nav-to-favourites", {}));
	}

	newHero(e) {
		console.log("newPerson en heroes-sidebar")
		console.log("Se va a crear un heroe");
		this.classNameField = "form-control";
		this.dispatchEvent(new CustomEvent("new-hero", {}));
	}

  startFight(e) {
		console.log("startFight en heroes-sidebar")
		console.log("Se va a empezar una pelea");
		this.classNameField = "form-control";
		this.dispatchEvent(new CustomEvent("start-fight", {}));
	}

	updated(changedProperties) {
		console.log("updated en heroes-sidebar");

		if(changedProperties.has("activePillId")) {
			console.log("Ha cambiado la propiedad activePillId en heroes-sidebar");
			this.updatePillsClasses();
		}
	}

	updatePillsClasses() {
		console.log("updatePillsClasses en heroes-sidebar");
		console.log("activePillId: " + this.activePillId);
		switch (this.activePillId) {
			case "v-pills-choosen-tab":
				this.shadowRoot.getElementById("v-pills-choosen-tab").className = "nav-link text-dark font-italic bg-light active";
				this.shadowRoot.getElementById("v-pills-filters-tab-heroes").className = "nav-link text-dark font-italic bg-light";
				this.shadowRoot.getElementById("v-pills-filters-tab-villains").className = "nav-link text-dark font-italic bg-light";
				this.shadowRoot.getElementById("v-pills-hero-tab").className = "nav-link text-dark font-italic bg-light";
				break;

			case "v-pills-filters-tab-heroes":
				this.shadowRoot.getElementById("v-pills-choosen-tab").className = "nav-link text-dark font-italic bg-light";
				this.shadowRoot.getElementById("v-pills-filters-tab-heroes").className = "nav-link text-dark font-italic bg-light active";
				this.shadowRoot.getElementById("v-pills-filters-tab-villains").className = "nav-link text-dark font-italic bg-light";
				this.shadowRoot.getElementById("v-pills-hero-tab").className = "nav-link text-dark font-italic bg-light";
				break;

			case "v-pills-filters-tab-villains":
				this.shadowRoot.getElementById("v-pills-choosen-tab").className = "nav-link text-dark font-italic bg-light";
				this.shadowRoot.getElementById("v-pills-filters-tab-heroes").className = "nav-link text-dark font-italic bg-light";
				this.shadowRoot.getElementById("v-pills-filters-tab-villains").className = "nav-link text-dark font-italic bg-light active";
				this.shadowRoot.getElementById("v-pills-hero-tab").className = "nav-link text-dark font-italic bg-light";
				break;

			case "v-pills-hero-tab":
				this.shadowRoot.getElementById("v-pills-choosen-tab").className = "nav-link text-dark font-italic bg-light";
				this.shadowRoot.getElementById("v-pills-filters-tab-heroes").className = "nav-link text-dark font-italic bg-light";
				this.shadowRoot.getElementById("v-pills-filters-tab-villains").className = "nav-link text-dark font-italic bg-light";
				this.shadowRoot.getElementById("v-pills-hero-tab").className = "nav-link text-dark font-italic bg-light active";
				break;

			default:
				break;
		  }
	}
}

customElements.define('heroes-sidebar', HeroesSidebar);
