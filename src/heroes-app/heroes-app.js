import { LitElement, html } from 'lit-element'; //por defecto mira en node-modules
import '../heroes-main/heroes-main.js'
import '../heroes-header/heroes-header.js';
import '../heroes-footer/heroes-footer.js';
import '../heroes-sidebar/heroes-sidebar.js';
import '../heroes-lucha-resultado/heroes-lucha-resultado.js';

class HeroesApp extends LitElement {

    static get properties() {
        return {

        };
    }

    constructor(){
        super(); //Llamamos al constuctor de la clase base, en este caso de LitElements.
    }

    render() {

        //@change se ejecuta al perder el foco del campo
        //@input se ejecuta al teclear
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <heroes-header></heroes-header>
            <div class="container">
              <div class="row">
                  <heroes-sidebar class="col-12 col-md-4 col-lg-3"
                    @new-hero="${this.newHero}"
                    @start-fight="${this.startFight}"
                    @nav-to-favourites="${this.showFavourites}"
                    @nav-to-filters="${this.filterHeroes}"></heroes-sidebar>
                  <heroes-main class="col" @hero-updated="${this.heroUpdated}"></heroes-main>
              </div>
            </div>
            <heroes-footer></heroes-footer>
        `;
    }

    heroUpdated(e){
      console.log("heroUpdated heroes-app");

      this.shadowRoot.querySelector('heroes-sidebar').emptyHeroes = e.detail.emptyHeroes;
    }

    newHero(e) {
      console.log('heroes-app - newPerson');
      this.shadowRoot.querySelector('heroes-main').showHeroesForm = true;
    }

    startFight(e) {
      console.log('heroes-app - startFight');
      this.shadowRoot.querySelector('heroes-main').startFight();
    }

    showFavourites(){
      console.log('heroes-app - showFavourites');
      this.shadowRoot.querySelector('heroes-main').showFavourites();
    }

    filterHeroes(e) {
      console.log('heroes-app - filterHeroes');

      switch (e.detail.filter.fType) {
        case 'H': //Filtro para heroes
          console.log('Filtro para heroes');
          this.shadowRoot.querySelector('heroes-main').requestAllHeroes();
          break;
        case 'V':
          console.log('Filtro para villanos');
          this.shadowRoot.querySelector('heroes-main').requestAllVillanos();
          break;
        case 'A':
          console.log('Filtro para todo');
          this.shadowRoot.querySelector('heroes-main').requestAll();
          break;
        case 'N':
          console.log('Filtro por nombre');
          this.shadowRoot.querySelector('heroes-main').requestByName(e);
          break;
        default:
          console.log('El tipo de filtro no se corresponde a ninguno definido ' + e.detail.filter.fType + '.');
      }

    }
}

customElements.define('heroes-app', HeroesApp);
