import { LitElement, html, css } from 'lit-element' //por defecto mira en node-modules
class HeroesLuchaResultado extends LitElement {

    static get properties() {
        return {
            heroe1: {type: Object},
            heroe2: {type: Object},
            cuentaAtras: {type: Number}
        };
    }

    static get styles() {
      return css`
        .imgCuentaAtras {
          width: 100%;
        }

        .winner {
          animation: shake 5s;
          animation-iteration-count: infinite;
        }

        @keyframes shake {
          0% { transform: translate(1px, 1px) rotate(0deg); }
          10% { transform: translate(-1px, -2px) rotate(-1deg); }
          20% { transform: translate(-3px, 0px) rotate(1deg); }
          30% { transform: translate(3px, 2px) rotate(0deg); }
          40% { transform: translate(1px, -1px) rotate(1deg); }
          50% { transform: translate(-1px, 2px) rotate(-1deg); }
          60% { transform: translate(-3px, 1px) rotate(0deg); }
          70% { transform: translate(3px, 1px) rotate(-1deg); }
          80% { transform: translate(-1px, -1px) rotate(1deg); }
          90% { transform: translate(1px, 2px) rotate(0deg); }
          100% { transform: translate(1px, -2px) rotate(-1deg); }
        }
      `;
    }

    constructor(){
        super(); //Llamamos al constuctor de la clase base, en este caso de LitElements.
        this.heroe1 = {};
        this.heroe2 = {};
        this.heroe1.images = {};
        this.heroe2.images = {};
        this.cuentaAtras = 10;
    }

    updated(changedProperties){
      console.log("updated heroes-lucha-resultado");

      if (changedProperties.has("heroe1") || changedProperties.has("heroe2")){
          console.log("Han cambiado el valor de la propiedad heroes en updated heroes-lucha-resultado");
          this.inicializarPantalla();
          if (Object.keys(this.heroe1).length > 1 && Object.keys(this.heroe2).length > 1){
            this.calculaGanador();
          }
      }
    }

    inicializarPantalla(){
      this.cuentaAtras = 10;
      this.shadowRoot.getElementById("Heroe1Foto").classList.remove("d-none");
      this.shadowRoot.getElementById("Heroe2Foto").classList.remove("d-none");
      this.shadowRoot.getElementById("vsImage").classList.remove("d-none");
      this.shadowRoot.getElementById("divLucha").classList.remove("d-none");
      this.shadowRoot.getElementById("youWin").classList.add("d-none");
      this.shadowRoot.getElementById("imgCuentaAtras").src = "";
      this.shadowRoot.getElementById("Heroe1Foto").classList.remove("winner")
      this.shadowRoot.getElementById("Heroe2Foto").classList.remove("winner")

    }

    render() {
        //@change se ejecuta al perder el foco del campo
        //@input se ejecuta al teclear
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <div class="d-flex justify-content-center bg-dark text-center rounded row" @click="${this.volver}">
              <img id="youWin" src="./img/YouWin.png" alt="You Win" height="150px" width="300px" class="img-fluid d-block d-none d-print-none col-12 col-lg-4">
              <img id="Heroe1Foto" src="${this.heroe1.images.sm}" title="${this.heroe1.name}" alt="${this.heroe1.name}" class="rounded border border-5 border-danger col-12 col-lg-4">
              <img id="vsImage" src="./img/street-fighter-vs-png-illustration.png" alt="Street Fighter VS"  class="img-fluid d-block col-12 col-lg-3">
              <img id="Heroe2Foto" src="${this.heroe2.images.sm}" title="${this.heroe2.name}" alt="${this.heroe2.name}" class="rounded border border-5 border-success col-12 col-lg-4">
            </div>
            <br/><br/>
            <div id="divLucha" class="d-flex justify-content-center bg-dark text-center row">
              <div class="badge bg-primary text-wrap col">
                <p class="fw-bolder fs-1">Fighting ${this.cuentaAtras}</p>
              </div>
            </div>
            <div class="d-flex justify-content-center bg-dark text-center row">
              <div class="col-12">
                <img id="imgCuentaAtras" src=""  class="imgCuentaAtras d-flex justify-content-center bg-dark text-center img-responsive">
              </div>
            </div>
        `;
    }

    calculaGanador()
    {
      console.log("CalulaGanador");
      let heroe1Media = 0;
      let heroe2Media = 0;

      heroe1Media = (this.heroe1.powerstats.intelligence +
        this.heroe1.powerstats.strength +
        this.heroe1.powerstats.speed +
        this.heroe1.powerstats.durability +
        this.heroe1.powerstats.power +
        this.heroe1.powerstats.combat) / 6;

      heroe2Media = (this.heroe2.powerstats.intelligence +
        this.heroe2.powerstats.strength +
        this.heroe2.powerstats.speed +
        this.heroe2.powerstats.durability +
        this.heroe2.powerstats.power +
        this.heroe2.powerstats.combat) / 6;

      if (heroe1Media > heroe2Media){
        this.heroe1.winner = true;
        this.heroe2.winner = false;
      }else{
        this.heroe1.winner = false;
        this.heroe2.winner = true;
      }

      this.cuentaAtras = 10;

      this.interval = window.setInterval(() => {
        console.log("mostrarganador " + this.cuentaAtras);

        if (this.cuentaAtras === 7){
          this.shadowRoot.getElementById("imgCuentaAtras").src = "./img/KATAKROK.png";
        }
        if (this.cuentaAtras === 4){
          this.shadowRoot.getElementById("imgCuentaAtras").src = "./img/KATAKROKER.png";
        }
        if (this.cuentaAtras === 0){
          this.shadowRoot.getElementById("divLucha").classList.add("d-none");
          this.shadowRoot.getElementById("imgCuentaAtras").src = "./img/KATAFINISH.png";
          this.shadowRoot.getElementById("vsImage").classList.add("d-none");
          this.shadowRoot.getElementById("youWin").classList.remove("d-none");
          if (this.heroe2.winner === true){
            this.shadowRoot.getElementById("Heroe1Foto").classList.add("d-none");
            this.shadowRoot.getElementById("Heroe2Foto").classList.add("winner");
          }else{
            this.shadowRoot.getElementById("Heroe2Foto").classList.add("d-none");
            this.shadowRoot.getElementById("Heroe1Foto").classList.add("winner");
          }


          window.clearInterval(this.interval);


        }

        if (this.cuentaAtras > 0)
          --this.cuentaAtras;
        }, 1000);
    }

    volver(){

    }

    disconnectedCallback() {
      super.disconnectedCallback();
      window.clearInterval(this.interval);
    }

}

customElements.define('heroes-lucha-resultado', HeroesLuchaResultado);
