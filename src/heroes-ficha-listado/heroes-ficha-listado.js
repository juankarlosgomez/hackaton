import { LitElement, html, css } from 'lit-element' //por defecto mira en node-modules

class HeroesFichaListado extends LitElement {

    static get styles() {
      return css `
        .imgRedonda {
          width:30px;
          height:30px;
          border-radius:160px;
          border:2px solid black;
          }
      `;
    }

    static get properties() {
        return {
            heroe: {type: Object},
            isFavourite: {type: Boolean}
        };
    }

    constructor(){
        super(); //Llamamos al constuctor de la clase base, en este caso de LitElements.
        this.heroe = {};
        this.isFavourite = false;
    }

    //Evento litElemnts
    updated(changedProperties){
      console.log("updated HeroesFichaListado");

      if (changedProperties.has("heroe")){
          console.log("Han cambiado el valor de la propiedad heroes en heroes-main");

          if(this.heroe.fav && this.heroe.fav === true){
            this.isFavourite = true;
          }
      }
    }

    render() {

        //@change se ejecuta al perder el foco del campo
        //@input se ejecuta al teclear
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <div class="card h-100" style="margin-bottom:2em">
              <img class="card-img-top rounded " src="${this.heroe.images.sm}" height="180" alt="${this.heroe.name}">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title"><strong>${this.heroe.name}</strong></h5>
                  </div>
                  <div class="col">
                    <div class="float-end">
                      ${this.heroe.biography.alignment === "good" ?
                        html`<img class="imgRedonda" src="./img/hero.png" title="Hero" alt="Hero">` :
                        html`<img class="imgRedonda" src="./img/villian.png" title="Villian" alt="Villian">`
                      }
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    ${this.heroe.biography.fullName}
                  </div>
                  <div class="col">
                    <div class="float-end">
                      ${this.isFavourite ?
                        html`<img @click="${this.removeFavourite}" src="./img/Favourite.png" title="Click to remove from favourites" alt="Escudo capitán desactivado">` :
                        html`<img @click="${this.addFavourite}" src="./img/NoFavourite.png" title="Click to add to favourites" alt="Escudo capitán activado">`
                      }
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item"><span class="${this.heroe.powerstats.intelligence <= 30 ? "text-danger": this.heroe.powerstats.intelligence > 30 && this.heroe.powerstats.intelligence < 70 ? "text-warning":  "text-success"}"><strong>${this.heroe.powerstats.intelligence}</strong></span> Intelligence</li>
                      <li class="list-group-item"><span class="${this.heroe.powerstats.strength <= 30 ? "text-danger": this.heroe.powerstats.strength > 30 && this.heroe.powerstats.strength < 70 ? "text-warning":"text-success"}"><strong>${this.heroe.powerstats.strength}</strong></span> Strength</li>
                      <li class="list-group-item"><span class="${this.heroe.powerstats.speed <= 30 ? "text-danger": this.heroe.powerstats.speed > 30 && this.heroe.powerstats.speed < 70 ? "text-warning":"text-success"}"><strong>${this.heroe.powerstats.speed}</strong></span> Speed</li>
                      <li class="list-group-item"><span class="${this.heroe.powerstats.durability <= 30 ? "text-danger": this.heroe.powerstats.durability > 30 && this.heroe.powerstats.durability < 70 ? "text-warning":"text-success"}"><strong>${this.heroe.powerstats.durability}</strong></span> Durability</li>
                      <li class="list-group-item"><span class="${this.heroe.powerstats.power <= 30 ? "text-danger": this.heroe.powerstats.power > 30 && this.heroe.powerstats.power < 70 ? "text-warning":"text-success"}"><strong>${this.heroe.powerstats.power}</strong></span> Power</li>
                      <li class="list-group-item"><span class="${this.heroe.powerstats.combat <= 30 ? "text-danger": this.heroe.powerstats.combat > 30 && this.heroe.powerstats.combat < 70 ? "text-warning":"text-success"}"><strong>${this.heroe.powerstats.combat}</strong></span> Combat</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="card-footer text-center">
                <slot></slot>
              </div>
            </div>
        `;
    }

    removeFavourite(e){
        console.log("removeFavourite in heroes-ficha-listado");
        e.preventDefault();
        e.cancelBubble = true;

        this.dispatchEvent(
            new CustomEvent(
              "heroes-ficha-listado-remove-favourite",{
                detail: {
                  id: this.heroe.id
                }
              }
            )
          );

    }

    addFavourite(e){
        console.log("addFavourite in heroes-ficha-listado");
        e.preventDefault();
        e.cancelBubble = true;

        this.dispatchEvent(
            new CustomEvent(
              "heroes-ficha-listado-add-favourite",{
                detail: {
                  id: this.heroe.id
                }
              }
            )
          );


    }

}

customElements.define('heroes-ficha-listado', HeroesFichaListado);
