import { LitElement, html } from 'lit-element';

class HeroesFooter extends LitElement {

  static get properties() {
    return {

    };
  }

  constructor() {

    super();

  }


  render() {
    return html`
        <div>
          <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
            integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
          <img src="./img/pie.jpg" class="img-fluid mx-auto d-block">
        </div>
      `;
  }

}

customElements.define('heroes-footer', HeroesFooter);
